#!/usr/bin/env python
# coding: utf-8

# In[196]:


import pandas as pd
from fuzzywuzzy import fuzz
import re
import html2text
import nltk
from nltk.tokenize import word_tokenize
from fuzzysearch import find_near_matches
import os
import phonenumbers
import time


# In[197]:


df = pd.read_csv("doc_info.csv")


# In[198]:


df


# In[182]:


start = time.time()
df_final = pd.DataFrame()
for doc in os.listdir('Scrapped/'):
    name = doc 
    address = list(df[df['Provider Name'] == doc]['Address'])[0]
    contact = phonenumbers.format_number(phonenumbers.parse(list(df[df['Provider Name'] == doc]['Contact'])[0], "US"), phonenumbers.PhoneNumberFormat.E164)
    
    links = []
    for html in os.listdir('Scrapped/' + str(doc) + '/'):
        if "html" in html:
            links.append(html)
    
    for f in links:
        
        path = "Scrapped/"+name+"/"+str(f)
        
        file = ""
        file =  open(path, "r",  encoding="utf8").read()
        h = html2text.HTML2Text()
        h.ignore_links = True
        h.skip_internal_links = True
        h.ignore_images = True
        read_file = h.handle(file)
        
        #For address:
        addr_set = []
        len_attr = int(min(max(1, len(address)//2), 10))
        occur = find_near_matches(address, read_file, max_l_dist=len_attr)
        for ele in occur:
            if ele.matched not in  addr_set:
                addr_set.append(ele.matched)
        
        
        #For ph number:
        ph_set = []
        for match in phonenumbers.PhoneNumberMatcher(file, "US"):
            ele = phonenumbers.format_number(match.number, phonenumbers.PhoneNumberFormat.E164)
            if ele not in ph_set:
                ph_set.append(ele)
         
        
        temp = pd.DataFrame([ph_set, addr_set]).T
        r = []
        for i in range(len(temp)):
            r.append(doc) 
        temp = pd.concat([pd.DataFrame(r), temp], axis=1)
        r = []
        for i in range(len(temp)):
            r.append(f) 
        final = pd.concat([pd.DataFrame(r), temp], ignore_index=True, axis=1)
        
        
        pth = "files/" + doc + ".csv"
        df_final = pd.concat([df_final, final], axis=0)
df_final.columns = ['Website', "Provider", "Contact", "Address"]
df_final.to_csv("files/final2.csv", index = False)
end = time.time()
print(end - start)


# In[184]:


file =  open("Scrapped/Dr. Cheryl Tam Chu/doctor.webmd.com.html", "r",  encoding="utf8").read()
h = html2text.HTML2Text()
# h.ignore_links = True
# h.skip_internal_links = True
# h.ignore_images = True
read_file = h.handle(file)
ph_set = []
for match in phonenumbers.PhoneNumberMatcher(read_file, "US"):
    ele = phonenumbers.format_number(match.number, phonenumbers.PhoneNumberFormat.E164)
    if ele not in ph_set:
        ph_set.append(ele)


# In[185]:


ph_set


# In[189]:


df.sort_values("Provider Name")


# In[195]:





# In[200]:


from postal.parser import parse_address


# In[203]:


get_ipython().system('conda install -c conda-forge postal')


# In[ ]:




